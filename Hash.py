import hashlib


def sha_256(data):
    # Создаем объект
    hash_code = hashlib.sha256()

    # Обновляем хеш

    hash_code.update(data.encode('utf-8'))

    # Получаем и возвращаем хеш в шестнадцатеричном формате
    return hash_code.hexdigest()
