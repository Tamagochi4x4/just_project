from fastapi import FastAPI, HTTPException, Depends
from fastapi.responses import HTMLResponse
from pydantic import BaseModel
import asyncio
import uvicorn
from datetime import datetime, timedelta
from Data_Base import coll_data
from Hash import sha_256

app = FastAPI()

employees = {
    "Agata": {"password": "2310", "salary": 1000, "promotion_date": "2023-10-23"},
    "Sergey": {"password": "1007", "salary": 1200, "promotion_date": "2023-07-10"},
    "Artem": {"password": "1104", "salary": 800, "promotion_date": "2023-04-11"},
    "Kenan": {"password": "1810", "salary": 15000, "promotion_date": "2023-10-18"},
}

class LoginData(BaseModel):
    username: str
    password: str

html_home = '''
        <html>
<head>
    <title>Service of your SALARY</title>
</head>
<body>
    <h1>Nice to meet you again!</h1>
    <h2>Click <a href="http://127.0.0.1:8000/docs">http://127.0.0.1:8000/docs</a> for check <h2>


</body>
</html>
    '''

# Здес хранятся зарегистрировашиеся токены и дата их конца действия
tokens = {}


def new_token(username: str, password: str) -> str:
    # Создание нового токена на 30 минут
    # Здесь располагается хеш-функция
    token = sha_256(username + password)
    expiration = datetime.now() + timedelta(minutes=60)
    tokens[token] = expiration
    return token


# Проверка токена на сущуствование
def check_token(token: str):
    if token not in tokens:
        raise HTTPException(status_code=401, detail="Invalid token")
    now = datetime.now()
    if tokens[token] < now:
        raise HTTPException(status_code=401, detail="Token expired")
    return token


# Post авторизация токена
@app.post("/token")
async def get_token(login_data: LoginData):
    username = login_data.username
    password = login_data.password
    if username not in employees or password != employees[username]["password"]:
        raise HTTPException(status_code=401, detail="Неправильное имя пользователя или пароль")

    token = new_token(username, password)

    return {"token": token}


# На главную
@app.get("/")
async def get():
    return HTMLResponse(html_home)


# Инф. о зарплате
# Изначально мы проверяем дейтсвует ли еще токен
@app.get("/salary/{username}")
async def get_salary(username: str, token: str = Depends(check_token)):
    if token not in tokens:
        raise HTTPException(status_code=401, detail="Токен недействиетелен ...")

    salary = await asyncio.to_thread(lambda: employees[username]["salary"])

    return {"name": username,
            "salary": salary}


# Информация о дате повышение
# Здесь тоже мы проверяем дейтсвует ли еще токен
@app.get("/promotion_date/{username}")
async def get_promotion_date(username: str, token: str = Depends(check_token)):
    if token not in tokens:
        raise HTTPException(status_code=401, detail="Токен недействителен ...")

    promotion_date = await asyncio.to_thread(lambda: employees[username]["promotion_date"])

    return {"name": username,
            "promotion_date": promotion_date}


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
